module github.com/ConorNevin/traceable

go 1.16

require (
	github.com/frankban/quicktest v1.14.0
	github.com/jstemmer/go-junit-report v0.9.1
	github.com/mattn/goveralls v0.0.11
	github.com/opentracing/opentracing-go v1.2.0
	golang.org/x/mod v0.5.1
	golang.org/x/tools v0.1.8
)
